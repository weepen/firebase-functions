import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import * as algolia from 'algoliasearch'
import Axios, * as axios from 'axios'
import { deleteCollection } from './collections'
import { calendar_v3, google } from 'googleapis'

admin.initializeApp()

const firestore = admin.firestore()
const firestoreSettings = {timestampsInSnapshots: true}
firestore.settings(firestoreSettings)
const storage = admin.storage()
const messaging = admin.messaging()

const ALGOLIA_APP_ID = functions.config().algolia.app_id
const ALGOLIA_API_KEY = functions.config().algolia.api_key
const ALGOLIA_EVENTS_INDEX_NAME = 'dev_firebase_events'
const ALGOLIA_PLACES_INDEX_NAME = 'dev_firebase_places'
const algoliaClient = algolia(ALGOLIA_APP_ID, ALGOLIA_API_KEY)

const OPENWEATHER_API_APP_ID = functions.config().openweather.app_id

class AlgoliaPlaceRecord {
  private _sport: FirebaseFirestore.DocumentReference
  r: placeRecord

  constructor(objID: string, d: FirebaseFirestore.DocumentData) {
    this.r = {
      objectID: objID,
      address: d.address,
      description: d.description,
      _geoloc: {
        lat: d.geoloc.latitude,
        lng: d.geoloc.longitude
      },
      ownerUID: d.owner.id,
      reservation: d.reservation,
      sportUID: d.sport.id,
    }
    this._sport = d.sport
  }
  async updateSportName() {
    // Retrieve sport name
    try {
      const sportSnap = await this._sport.get()
      this.r.sport = sportSnap.get('name')
    } catch (err) {
      console.error('could not access to sport name:', err)
    }
  }
  record(): placeRecord {
    return this.r
  }
}

class AlgoliaEventRecord {
  private _sport: FirebaseFirestore.DocumentReference
  r: eventRecord

  constructor(objID: string, d: FirebaseFirestore.DocumentData) {
    this.r = {
      objectID: objID,
      description: d.description,
      endDate: Math.floor(d.endDate / 1000),
      _geoloc: {
        lat: d.geoloc.latitude,
        lng: d.geoloc.longitude
      },
      address: {
        city: d.address.city,
        country: d.address.country,
        zip: d.address.zip,
        name: d.address.name,
        thoroughfare: d.address.thoroughfare,
      },
      reservation: {
        currency: d.reservation.currency,
        price: d.reservation.price
      },
      startDate: Math.floor(d.startDate / 1000),
      title: d.title,
      maxParticipants: d.maxParticipants,
      minParticipants: d.minParticipants,
      ownerUID: d.owner.id,
      private: d.private,
      sponsored: d.sponsored,
      sportUID: d.sport.id,
    }
    this._sport = d.sport
  }

  async updateSportName() {
    // Retrieve sport name
    try {
      const sportSnap = await this._sport.get()
      this.r.sport = sportSnap.get('name')
    } catch (err) {
      console.error('could not access to sport name:', err)
    }
  }
  record(): eventRecord {
    return this.r
  }
}

async function unregisterUserFromEvents(userUid) {
  try {
    const promises = []
    const eventsSnapshot = await firestore.collection(`weepen/dev/events`).get()
    eventsSnapshot.forEach(async (event) => {
      const participantsSnapshot = await event.ref.collection('participants').get()
      participantsSnapshot.forEach(participant => {
        if (userUid === participant.data().ref.id) {
          promises.push(participant.ref.delete())
        }
      })
    })
    return Promise.all(promises)
  }
  catch (err) {
    return err
  }
}

export const userCreate = functions.firestore.document('weepen/dev/users/{userId}').onCreate(async (snap, context) => {
  return messaging.subscribeToTopic(snap.get('registrationToken'), 'user_' + context.params.userId)
})

export const userDelete = functions.auth.user().onDelete(async (user) => {

  const userRecord = firestore.doc(`weepen/dev/users/${user.uid}`)

  // cancel user bookings
  const p0 = []
  try {
    const bookings = await userRecord.collection('bookings').get()
    bookings.forEach(async (doc) => {
      const bk = await doc.data().ref.get()
      p0.push(authorize(removeGCalEvent, {
        gcalEventId: bk.get('gcalEventId'),
        placeRef: bk.get('place')
      }))
    })
  } catch (err) {
    console.error('could not remove gcal events:', err)
    return err
  }

  // Delete the user's sports and visibility collections and cancel bookings
  // delete bookings records
  // and finally delete the user's document
  const p1 = Promise.all([
    deleteCollection(firestore, userRecord.collection('visibility/PUBLIC/sports'), 20),
    deleteCollection(firestore, userRecord.collection('visibility'), 4),
    p0
  ]).then(() => {
    // delete user bookings records
    return deleteCollection(firestore, userRecord.collection('bookings'), 4)
  }).then(() => {
    return userRecord.delete()
  })

  // delete user's files
  const p2 = storage.bucket().deleteFiles({ prefix: `users/${user.uid}/` })

  // delete user's events
  const p3 = firestore.collection(`weepen/dev/events`).where('owner', '==', userRecord).get()
    .then(snapshot => {
      const promises = []
      snapshot.forEach(doc => {
        promises.push(doc.ref.delete())
      })
      return Promise.all(promises)
    }).catch(err => {
      console.error(`could not get ${user.uid} events:`, err)
    })

  const p4 = unregisterUserFromEvents(user.uid)

  // unsubscribe user from its unique topic
  let userSnap: FirebaseFirestore.DocumentSnapshot
  try {
    userSnap = await userRecord.get()
  } catch (err) {
    console.error('could not get user snap:', err)
  }
  const p5 = messaging.unsubscribeFromTopic(userSnap.get('registrationToken'), 'user_' + user.uid)

  return Promise.all([p1, p2, p3, p4, p5])
});

async function insertEventInAlgolia(eventID, event) {
  const eventsIndex = algoliaClient.initIndex(ALGOLIA_EVENTS_INDEX_NAME)
  const aer = new AlgoliaEventRecord(eventID, event)
  await aer.updateSportName()
  return eventsIndex.saveObject(aer.record())
}

export const newEvent = functions.firestore.document('weepen/dev/events/{eventId}').onCreate(async (snap, context) => {
  const eventID = context.params.eventId
  const event = snap.data()
  const promises = []

  // create a chat document
  promises.push(firestore.doc(`/weepen/dev/chat/${snap.id}`).set({ event: snap.ref }))

  // insert in Algolia
  promises.push(insertEventInAlgolia(eventID, snap.data()))

  try {
    // add the event's owner as a participant
    // the event nbParticipant will be updated by updateNbParticipantsEventInAlgolia()
    const ownerDoc = await firestore.doc(`weepen/dev/events/${eventID}/participants/${event.owner.id}`)
    promises.push(ownerDoc.set({ ref: event.owner }))
  } catch (err) {
    console.error('could not add the event owner as a participant: ', err)
    return err
  }

  return Promise.all(promises)
})

export const newPlace = functions.firestore.document('weepen/dev/places/{placeId}').onCreate(async (snap, context) => {
  const placeID = context.params.placeId
  const place = snap.data()
  const placesIndex = algoliaClient.initIndex(ALGOLIA_PLACES_INDEX_NAME)
  const apr = new AlgoliaPlaceRecord(placeID, place)
  await apr.updateSportName()
  return placesIndex.saveObject(apr.record())
})

export const deleteEvent = functions.firestore.document('weepen/dev/events/{eventId}').onDelete(async (snap, context) => {
  const eventID = context.params.eventId
  const eventsIndex = algoliaClient.initIndex(ALGOLIA_EVENTS_INDEX_NAME)
  const promises = []

  try {
    // delete the related chat
    const chatsSnap = await firestore.collection(`weepen/dev/chat`).where('event', '==', snap.ref).get()
    chatsSnap.forEach(async doc => {
      promises.push(deleteCollection(firestore, doc.ref.collection('messages'), 20).then(() => { return doc.ref.delete() }))
    })
  } catch (err) {
    console.error('could not delete related chat document')
    return err
  }

  promises.push(deleteCollection(firestore, firestore.collection(`weepen/dev/events/${eventID}/participants`), 10))

  // delete gcal event if a booking was attached to the event
  if (snap.data().booking !== undefined) {
    let bkSnap: FirebaseFirestore.DocumentSnapshot
    try {
      bkSnap = await snap.data().booking.get()
    } catch (err) {
      console.error('could not get booking snap')
      return err
    }
    if (bkSnap !== undefined && bkSnap.exists) {
      try {
        // delete from gcal
        await authorize(removeGCalEvent, {
          gcalEventId: bkSnap.get('gcalEventId'),
          placeRef: bkSnap.get('place')
        })
      } catch (err) {
        console.error('could not remove booking from gcal:', err)
        return err
      }
    }
  }

  // publish to topic events
  promises.push(messaging.send({
    topic: 'event_' + eventID,
    data: {
      scope: 'event',
      action: 'delete',
      eventId: eventID,
    }
  }))

  promises.push(eventsIndex.deleteObject(eventID))

  return Promise.all(promises)
})

export const deletePlace = functions.firestore.document('weepen/dev/places/{placeId}').onDelete((snap, context) => {
  const placeID = context.params.placeId
  const placesIndex = algoliaClient.initIndex(ALGOLIA_PLACES_INDEX_NAME)
  return placesIndex.deleteObject(placeID)
})


export const participantWrite = functions.firestore.document('weepen/dev/events/{eventId}/participants/{participantId}').onWrite(async (change, context) => {
  const eventID = context.params.eventId
  const promises = []

  // event's participants FCM sub/unsub event topic
  if (change.after.exists) {
    try {
      // subscribe user to the event topic
      const user = await change.after.data().ref.get()
      promises.push(messaging.subscribeToTopic(user.get('registrationToken'), 'event_' + eventID))
    } catch (e) {
      console.error('could not subscribe user to event topic: ', e)
    }
  } else if (!change.after.exists) {
    // unsubscribe user from the event topic
    try {
      const user = await change.before.data().ref.get()
      return messaging.unsubscribeFromTopic(user.get('registrationToken'), 'event_' + eventID)
    } catch (e) {
      console.error('could not unsubscribe user from event topic: ', e)
    }
  }

  let eventDoc: FirebaseFirestore.DocumentData
  try {
    const eventSnap = await firestore.doc(`weepen/dev/events/${eventID}`).get()
    eventDoc = eventSnap.data()
  } catch (err) {
    console.error('could not get event:', err)
    return err
  }

  // Stop here if event exists and is archived
  if (change.after.exists && eventDoc.archived) {
    return Promise.all(promises)
  }

  // update nbParticipants in Algolia
  let nbParticipants: number
  try {
    const participantsSnap = await change.after.ref.parent.get()
    nbParticipants = participantsSnap.size
  } catch (err) {
    console.error('could not get participants collection:', err)
    return err
  }
  const eventsIndex = algoliaClient.initIndex(ALGOLIA_EVENTS_INDEX_NAME)
  promises.push(eventsIndex.partialUpdateObject({
    objectID: eventID,
    nbParticipants: nbParticipants,
  }))

  /*
  // notify event's owner if minumum participants reached
  if (nbParticipants === eventDoc.minParticipants) {
    try {
      const ownerSnap = await eventDoc.owner.get()
      console.log('owner data', ownerSnap.data())
      promises.push(messaging.send({
        token: ownerSnap.get('registrationToken'),
        data: {
          scope: 'event',
          action: 'minParticipantsReached',
          eventId: eventID,
        }
      }))
    } catch (err) {
      console.error('could not notify event owner of min reached:', err)
      return err
    }
  }
  */

  return Promise.all(promises)
})

export const newChatMessage = functions.firestore.document('weepen/dev/chat/{chatId}/messages/{msgId}').onCreate(async (msgSnap, context) => {
  const chatID = context.params.chatId

  return firestore.doc(`weepen/dev/chat/${chatID}`).get().then(async (chatDoc) => {
    const eventID = chatDoc.get('event').id
    const msg = msgSnap.data()

    // Do not send the new msg notif to the sender
    const condition = "('event_" + eventID + "' in topics) && !('user_" + msg.sender.id + "' in topics)"

    return messaging.send({
      condition: condition,
      //topic: 'event_' + eventID,
      data: {
        scope: 'chat',
        action: 'newMsg',
        body: msg.message,
        senderId: msg.sender.id,
        eventId: eventID
      }
    })
  }).catch((err) => {
    console.error('could not notify participants of a new message:', err)
    return err
  })
})

export const bookingEventNotify = functions.firestore.document('weepen/dev/events/{eventId}').onWrite((change, context) => {
  const eventID = context.params.eventId
  const bookingBefore = change.before.get('booking')
  const bookingAfter = change.after.get('booking')

  let action: string

  if (bookingBefore === undefined && bookingAfter !== undefined) {
    action = 'bookingAdded'
  } else if (bookingBefore !== undefined && bookingAfter === undefined) {
    action = 'bookingDeleted'
  } else {
    return null
  }

  return messaging.send({
    topic: 'event_' + eventID,
    data: {
      scope: 'event',
      action: action,
      eventId: eventID
    }
  })

})

export const updateEvent = functions.firestore.document('weepen/dev/events/{eventId}').onUpdate(async (change, context) => {
  const eventID = context.params.eventId
  const oldEvent = change.before.data()
  const updatedEvent = change.after.data()
  const promises = []

  if (updatedEvent.archived) {
    return null
  }

  promises.push(messaging.send({
    topic: 'event_' + eventID,
    data: {
      scope: 'event',
      action: 'update',
      eventId: eventID
    }
  }))

  const aer = new AlgoliaEventRecord(eventID, updatedEvent)
  try {
    // Retrieve sport name if sport UID changed
    if (updatedEvent.sport.id !== oldEvent.sport.id) {
      await aer.updateSportName()
    }
  } catch (err) {
    console.error('could not update algolia event record:', err)
    return err
  }
  const eventsIndex = algoliaClient.initIndex(ALGOLIA_EVENTS_INDEX_NAME)
  promises.push(eventsIndex.partialUpdateObject(aer.record()))

  return Promise.all(promises)
})

export const updatePlace = functions.firestore.document('weepen/dev/places/{placeId}').onUpdate(async (change, context) => {
  const placeID = context.params.placeId
  const oldPlace = change.before.data()
  const updatedPlace = change.after.data()
  const apr = new AlgoliaPlaceRecord(placeID, updatedPlace)
  if (updatedPlace.sport.id !== oldPlace.sport.id) {
    await apr.updateSportName()
  }
  const placesIndex = algoliaClient.initIndex(ALGOLIA_PLACES_INDEX_NAME)
  return placesIndex.partialUpdateObject(apr.record())
})

async function deletePastEvents() {
  const promises = []

  // delete past events from Algolia
  const now = new Date()
  const nowTimestamp = Math.floor(now.getTime() / 1000)
  const eventsIndex = algoliaClient.initIndex(ALGOLIA_EVENTS_INDEX_NAME)
  promises.push(eventsIndex.deleteBy({
    filters: `endDate<${nowTimestamp}`,
  }))

  try {
    // Set 'archived' to true for all past events
    const pastEventsSnap = await firestore.collection(`weepen/dev/events`).where('endDate', '<', now).get()
    pastEventsSnap.forEach(doc => {
      promises.push(doc.ref.update({ 'archived': true }))
    })
  } catch (err) {
    console.error('could not set archived status for some events: ', err)
  }

  return Promise.all(promises)
}

async function getWeather(lat, lon) {
  const baseURL = 'http://api.openweathermap.org/data/2.5/forecast?'
  const appid = `APPID=${OPENWEATHER_API_APP_ID}`
  const location = `lat=${lat}&lon=${lon}`
  const unit = 'units=metric'
  const lang = 'lang=us'
  const cnt = 'cnt=11'
  const url = `${baseURL}${appid}&${location}&${unit}&${lang}&${cnt}`
  try {
    const ret = await Axios.get(url)
    return ret.data
  } catch (err) {
    return err
  }
}

async function weatherNotify() {

  const now = new Date()
  const inTwoDays = new Date()
  inTwoDays.setDate(now.getDate() + 2) // add 2 days to the date
  const promises = []

  try {
    // get events than begin in about 2 days
    const nearEventsSnap = await firestore.collection(`weepen/dev/events`)
      .where('archived', '==', false)
      .where('startDate', '>', now)
      .where('startDate', '<=', inTwoDays)
      .get()

    nearEventsSnap.forEach(async eDoc => {
      const event = eDoc.data()
      // get weather for the event's location
      const weather = await getWeather(event.geoloc.latitude, event.geoloc.longitude)

      // get the forecast as close as possible to startDate
      weather.list.sort((a, b) => {
        return Math.abs(event.startDate - a) - Math.abs(event.startDate - b)
      })
      const closestForecast = weather.list[0]

      // set weather in the event's chat
      promises.push(firestore.collection(`weepen/dev/chat/${eDoc.id}/messages`).add({
        date: now,
        message: `${closestForecast.main.temp}°, ${closestForecast.weather[0].description}`,
        sender: firestore.doc('weepen/dev/users/BOTCOACH'),
      }))
    })
  } catch (err) {
    console.error('could not notify participants of the weather forecasts', err)
    return err
  }
  return Promise.all(promises)
}

export const quarterlyJobs = functions.pubsub.topic('quarterly-tick').onPublish((event) => {
  return deletePastEvents()
});

export const dailyJobs = functions.pubsub.topic('daily-tick').onPublish((event) => {
  return weatherNotify()
});


// ---------------------

async function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const SCOPES = ['https://www.googleapis.com/auth/calendar'];

async function authorize(callback, data) {
  const oAuth2Client = new google.auth.OAuth2(
    functions.config().calendar.client_id,
    functions.config().calendar.client_secret,
    functions.config().calendar.redirect_uri
  )

  let token: undefined
  try {
    const miscDoc = await firestore.doc('weepen/misc').get()
    token = miscDoc.data().token
  } catch (e) {
    console.error('could not get misc document: ', e)
  }
  if (token === undefined) {
    return getAccessToken(oAuth2Client)
  }
  oAuth2Client.setCredentials(token)
  return callback(oAuth2Client, data)
}

// This function should be executed only once when no token is stored
async function getAccessToken(oAuth2Client) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  })
  console.log('Authorize this app by visiting this url:', authUrl,
    'once this is done, store the \'code\' in Firestore under weepen/misc')

  await delay(30000) // wait 30s (time for the us to authenticate and put the code in firestore)

  // get code in firestore
  const miscDoc = await firestore.doc('weepen/misc').get()
  const code = miscDoc.data().code

  oAuth2Client.getToken(code, async (err, token) => {
    if (err) {
      console.error('Error retrieving access token', err)
    }
    oAuth2Client.setCredentials(token)

    // store token in Firestore
    await firestore.doc('weepen/misc').set({ token: token })
  })
}

async function isFree(auth, wish): Promise<boolean> {
  let calendar: calendar_v3.Calendar
  calendar = google.calendar({
    version: 'v3',
    auth: auth,
  })

  try {
    const freeBusyRes = await calendar.freebusy.query({
      requestBody: {
        timeMin: wish.start,
        timeMax: wish.end,
        items: [{ id: wish.gcalId }]
      }
    })
    const state = freeBusyRes.data.calendars[wish.gcalId]
    if (state.errors !== undefined) {
      throw new Error('freeBusy error: ' + state.errors[0].reason)
    }
    if (state.busy.length === 0) {
      return true
    }
  } catch (err) {
    throw err
  }

  return false
}

async function addGCalEvent(auth, event) {
  let calendar: calendar_v3.Calendar
  calendar = google.calendar({
    version: 'v3',
    auth: auth,
  })

  const body = {
    summary: event.wish.title,
    description: event.wish.description,
    start: {
      dateTime: event.wish.start
    },
    end: {
      dateTime: event.wish.end
    }
  }
  let gcalEventId: string
  try {
    const insertionRes = await calendar.events.insert({
      calendarId: event.calendarId,
      requestBody: body
    })
    if (insertionRes.status !== 200) {
      console.error('could not insert event in gcal:', insertionRes.data)
      throw new Error('could not insert event in gcal, bad status code: ' + insertionRes.status)
    }
    gcalEventId = insertionRes.data.id
  } catch (err) {
    throw err
  }

  return gcalEventId
}

export const isPlaceAvailable = functions.https.onCall(async (data, context) => {
  if (!context.auth) throw new functions.https.HttpsError('unauthenticated', 'The function must be called while authenticated.')

  // get calendarID from place ID
  let calendarId: string
  const placeRef = firestore.doc(`weepen/dev/places/${data.placeId}`)
  try {
    const placeSnap = await placeRef.get()
    calendarId = placeSnap.get('calendarID')
  } catch (err) {
    console.error('could not get calendarId from place ' + data.placeId + ' :', err)
    throw new functions.https.HttpsError('not-found', 'could not get calendarId from specified place', err)
  }

  // Check place availability
  let isFreeRes: boolean
  try {
    isFreeRes = await authorize(isFree, {
      start: data.wish.start,
      end: data.wish.end,
      gcalId: calendarId,
    })
  } catch (err) {
    console.error('isFree error:', err)
    throw new functions.https.HttpsError('failed-precondition', 'could not get place availability', err.message)
  }
  if (!isFreeRes) {
    throw new functions.https.HttpsError('resource-exhausted', 'the place is not available at the specified time')
  }
})

export const booking = functions.https.onCall(async (data, context) => {
  if (!context.auth) throw new functions.https.HttpsError('unauthenticated', 'The function must be called while authenticated.')

  // check if event or user exists
  const eventRef = firestore.doc(`weepen/dev/events/${data.linkedTo.id}`)
  const userRef = firestore.doc(`weepen/dev/users/${data.linkedTo.id}`)
  if (data.linkedTo.type === 'event') {
    let eventSnap: FirebaseFirestore.DocumentSnapshot
    try {
      eventSnap = await eventRef.get()
    } catch (err) {
      console.error('could not get event snapshot', err)
      throw new functions.https.HttpsError('failed-precondition', 'could not get event snapshot', err)
    }
    if (!eventSnap.exists) throw new functions.https.HttpsError('not-found', 'the specified event does not exists')
  } else {
    let userSnap: FirebaseFirestore.DocumentSnapshot
    try {
      userSnap = await userRef.get()
    } catch (err) {
      console.error('could not get user snapshot', err)
      throw new functions.https.HttpsError('failed-precondition', 'could not get user snapshot', err)
    }
    if (!userSnap.exists) throw new functions.https.HttpsError('failed-precondition', 'the specified user does not exists')
  }

  // get calendarID from place ID
  let calendarId: string
  const placeRef = firestore.doc(`weepen/dev/places/${data.placeId}`)
  try {
    const placeSnap = await placeRef.get()
    calendarId = placeSnap.get('calendarID')
  } catch (err) {
    console.error('could not get calendarId from place ' + data.placeId + ' :', err)
    throw new functions.https.HttpsError('not-found', 'could not get calendarId from specified place', err)
  }

  // Check place availability
  let isFreeRes: boolean
  try {
    isFreeRes = await authorize(isFree, {
      start: data.wish.start,
      end: data.wish.end,
      gcalId: calendarId,
    })
  } catch (err) {
    console.error('isFree error:', err)
    throw new functions.https.HttpsError('failed-precondition', 'could not get place availability', err.message)
  }
  if (!isFreeRes) {
    throw new functions.https.HttpsError('resource-exhausted', 'the place is not available at the specified time')
  }

  // try to book on calendar
  let gcalEventId: string
  try {
    gcalEventId = await authorize(addGCalEvent, {
      calendarId: calendarId,
      wish: data.wish,
    })
  } catch (err) {
    console.error('could not book a place:', err)
    throw new functions.https.HttpsError('failed-precondition', 'booking was not possible', err)
  }

  // insert success in booking on Firestore
  let bookingRef: FirebaseFirestore.DocumentReference
  try {
    bookingRef = await firestore.collection('weepen/dev/bookings').add({
      place: placeRef,
      gcalEventId: gcalEventId,
      wish: data.wish,
      linkedTo: data.linkedTo
    })
  } catch (err) {
    console.error('could not insert booking info:', err)
    throw new functions.https.HttpsError('failed-precondition', 'inserting booking info failed', err)
  }

  // link booking to event or user
  try {
    if (data.linkedTo.type === 'event') {
      await eventRef.update({ booking: bookingRef })
    } else {
      await firestore.doc(userRef.path + `/bookings/${bookingRef.id}`).set({ ref: bookingRef })
    }
  } catch (err) {
    console.error('could not link booking:', err)
    throw new functions.https.HttpsError('failed-precondition', 'booking could not be linked', err)
  }

  return bookingRef.id
})

// bookingInfo {gcalEventId: , placeRef:}
async function removeGCalEvent(auth, bookingInfo) {
  let calendar: calendar_v3.Calendar
  calendar = google.calendar({
    version: 'v3',
    auth: auth,
  })

  let gcalEventId: string
  let calendarId: string

  // get booking object from ID
  try {
    gcalEventId = bookingInfo.gcalEventId
    const placeSnap = await bookingInfo.placeRef.get()
    calendarId = placeSnap.get('calendarID')
  } catch (err) {
    throw new Error('could not get booking place from its ID: ' + err)
  }

  // remove from gcal
  let deleteResStatus: number
  try {
    const deleteRes = await calendar.events.delete({
      calendarId: calendarId,
      eventId: gcalEventId,
    })
    deleteResStatus = deleteRes.status
  } catch (err) {
    throw new Error('gcal delete: ' + err.message)
  }

  if (deleteResStatus !== 204) {
    throw new Error('gcal delete event error, code ' + deleteResStatus)
  }
}

export const cancelBooking = functions.https.onCall(async (data, context) => {
  if (!context.auth) throw new functions.https.HttpsError('unauthenticated', 'The function must be called while authenticated.')
  const bookingId = data.bookingId
  const promises = []

  if (bookingId === undefined) throw new functions.https.HttpsError('invalid-argument', 'bookingId was not specified')

  // get booking document
  let bookingSnap: FirebaseFirestore.DocumentSnapshot
  try {
    bookingSnap = await firestore.doc(`weepen/dev/bookings/${bookingId}`).get()
  } catch (err) {
    console.error('could not get booking from Firestore:', err)
    throw new functions.https.HttpsError('failed-precondition', 'could not get booking from Firestore')
  }
  if (!bookingSnap.exists) throw new functions.https.HttpsError('invalid-argument', 'no booking with this ID exists')

  try {
    // delete from gcal
    await authorize(removeGCalEvent, {
      gcalEventId: bookingSnap.get('gcalEventId'),
      placeRef: bookingSnap.get('place')
    })
  } catch (err) {
    console.error('could not remove booking from gcal:', err)
    throw new functions.https.HttpsError('failed-precondition', 'booking cancellation was not possible', err.message)
  }

  // delete links
  const linkedTo = bookingSnap.get('linkedTo')
  if (linkedTo.type === 'event') {
    promises.push(firestore.doc(`weepen/dev/events/${linkedTo.id}`).delete())
  } else {
    promises.push(firestore.doc(`weepen/dev/users/${linkedTo.id}/bookings/${bookingId}`).delete())
  }

  // delete booking from Firestore
  promises.push(bookingSnap.ref.delete())

  Promise.all(promises).catch((err) => {
    console.error('could not cancel booking:', err)
    throw new functions.https.HttpsError('failed-precondition', 'booking cancellation was not possible', err)
  })
})