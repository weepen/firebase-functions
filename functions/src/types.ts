type eventRecord = {
    objectID: string
    description: string
    endDate: number
    _geoloc: {
        lat: number
        lng: number
    }
    address: {
        city: string
        zip: string
        country: string
        name: string
        thoroughfare: string
    }
    reservation: {
        currency: string
        price: number
    }
    sport?: string
    startDate: number
    title: string
    maxParticipants: number
    minParticipants: number
    ownerUID: string
    private: boolean
    sponsored: boolean
    sportUID: string
}

type placeRecord = {
    objectID: string
    address: {
        city: string
        zip: string
        country: string
        name: string
        thoroughfare: string
    }
    description: string
    _geoloc: {
      lat: number
      lng: number
    }
    ownerUID: string
    reservation: {
        currency: string
        hourlyRate: number
    }
    sportUID: string
    sport?: string
}